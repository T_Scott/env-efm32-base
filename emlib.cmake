########## Files ##########

# Set and add include locations
set(INCLUDES ${CMAKE_CURRENT_LIST_DIR}/platform/emlib/inc)
include_directories(SYSTEM ${INCLUDES})

# Set sources
FILE(GLOB SOURCES
	${CMAKE_CURRENT_LIST_DIR}/platform/emlib/src/*.c
)

########## Outputs ##########

# Add library
add_library(emlib ${SOURCES})
set(OPTIONAL_LIBS ${OPTIONAL_LIBS} emlib)

# Suppress errors
set_property(TARGET emlib APPEND_STRING PROPERTY COMPILE_FLAGS " -w")
