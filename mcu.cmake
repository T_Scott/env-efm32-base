# Expects { DEVICE } returns { MCU_FAMILY_U MCU_FAMILY_L }

message("\r\n********** MCU Family **********\r\n")

# Check we know which device we are building for
IF(NOT DEVICE)
    MESSAGE(FATAL_ERROR "DEVICE must be specified")
ENDIF()

message("Device: ${DEVICE}")

# Create upper and lower case device names
STRING(TOLOWER "${DEVICE}" EM_MCU_L)
STRING(TOUPPER "${DEVICE}" EM_MCU_U)

# Parse family from MCU name
STRING(REGEX MATCH "EFM32[BFMJP]G.." MCU_FAMILY ${EM_MCU_U})
IF(MCU_FAMILY STREQUAL "")
    STRING(REGEX REPLACE "^([A-Z]+[0-9]+[A-Z]+)[0-9]+[A-Z]+[0-9]+$" "\\1" MCU_FAMILY ${EM_MCU_U})
ENDIF()

message("Base Family: ${MCU_FAMILY}")

# Check we managed to extract a base
IF(MCU_FAMILY STREQUAL ${EM_MCU_U})
    MESSAGE(FATAL_ERROR "Not Valid MCU: " ${MCU_FAMILY})
ENDIF()

# Create upper and lower case base family names
STRING(TOLOWER "${MCU_FAMILY}" MCU_FAMILY_L)
STRING(TOUPPER "${MCU_FAMILY}" MCU_FAMILY_U)