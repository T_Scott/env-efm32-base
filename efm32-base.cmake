# Expects { DEVICE }

# Load processor options
include(${CMAKE_CURRENT_LIST_DIR}/mcu.cmake)

# Check we have acquired and modified required files for device
if(NOT ((${MCU_FAMILY_L} STREQUAL efm32zg) OR (${MCU_FAMILY_L} STREQUAL efm32pg1b)))
    message(FATAL_ERROR "Support files/Linker scripts not configured for ${DEVICE} family ${MCU_FAMILY_L}")
endif()

# Add modules
include(${CMAKE_CURRENT_LIST_DIR}/cmsis.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/device.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/emlib.cmake)

# Add compiler arguments for device
add_definitions(-D${DEVICE})

# Add linker and map args based on the project and device
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -T${DEVICE_LINKER_FILE_STRING} -Wl,-Map=${CMAKE_PROJECT_NAME}.map")

# Add arguments based on the core
if(MCU_FAMILY_U STREQUAL "EFM32ZG")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -mcpu=cortex-m0plus")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mcpu=cortex-m0plus")
    set(CMAKE_ASM_FLAGS "${CMAKE_ASM_FLAGS} -DLOOP_ADDR=0x8000 -mcpu=cortex-m0plus")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -mcpu=cortex-m0plus")
elseif((MCU_FAMILY_U STREQUAL "EFM32G") OR (MCU_FAMILY_U STREQUAL "EFM32LG") OR (MCU_FAMILY_U STREQUAL "EFM32JG1B"))
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -mcpu=cortex-m3 -mfix-cortex-m3-ldrd")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mcpu=cortex-m3 -mfix-cortex-m3-ldrd")
    set(CMAKE_ASM_FLAGS "${CMAKE_ASM_FLAGS} -DLOOP_ADDR=0x8000 -mcpu=cortex-m3 -mfix-cortex-m3-ldrd")
    set(CMAKE_EXE_LINKER_FLAGS "-mcpu=cortex-m3 -mfix-cortex-m3-ldrd ${CMAKE_EXE_LINKER_FLAGS}")
elseif((MCU_FAMILY_U STREQUAL "EFM32WG") OR (MCU_FAMILY_U STREQUAL "EFM32PG1B"))
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -mcpu=cortex-m4")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mcpu=cortex-m4")
    set(CMAKE_ASM_FLAGS "${CMAKE_ASM_FLAGS} -DLOOP_ADDR=0x8000 -mcpu=cortex-m4")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -mcpu=cortex-m4")
else()
    message(FATAL_ERROR "${MCU_FAMILY_U} core not specified")  
endif()

message("\r\n********** Updated Compiler Flags **********\r\n")

message("C Flags:\r\n${CMAKE_C_FLAGS}\r\n")
message("C++ Flags:\r\n${CMAKE_CXX_FLAGS}\r\n")
message("Assembly Flags:\r\n${CMAKE_ASM_FLAGS}\r\n")
message("Linker Flags:\r\n${CMAKE_EXE_LINKER_FLAGS}\r\n")