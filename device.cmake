# Expects { MCU_FAMILY_U MCU_FAMILY_L FLASH_START }

########## Files ##########

message("\r\n********** Device Setup Files **********\r\n")

# Check we set MCU family variable for configuration
if(NOT MCU_FAMILY_U)
    message(FATAL_ERROR "MCU family not set!")
endif()
if(NOT MCU_FAMILY_L)
    message(FATAL_ERROR "MCU family not set!")
endif()

# Set related directories
set(DEVICE_BASE_DIR "${CMAKE_CURRENT_LIST_DIR}/platform/Device/SiliconLabs/${MCU_FAMILY_U}")
set(DEVICE_INCLUDE_DIR "${DEVICE_BASE_DIR}/Include")
set(DEVICE_SOURCE_SUFFIX "/Source/")
set(DEVICE_SOURCE_DIR "${DEVICE_BASE_DIR}/${DEVICE_SOURCE_SUFFIX}")

message("Device base: ${DEVICE_BASE_DIR}")
message("Device source: ${DEVICE_SOURCE_SUFFIX}")

# Set system file
set(DEVICE_SYSTEM_FILE_NAME "system_${MCU_FAMILY_L}.c")
set(DEVICE_SYSTEM_FILE "${DEVICE_SOURCE_DIR}/${DEVICE_SYSTEM_FILE_NAME}")
message("System file: ${DEVICE_SYSTEM_FILE_NAME}")

# Set startup script
set(DEVICE_STARTUP_SCRIPT_NAME "/GCC/startup_${MCU_FAMILY_L}.S")
set(DEVICE_STARTUP_SCRIPT "${DEVICE_SOURCE_DIR}/${DEVICE_STARTUP_SCRIPT_NAME}")
message("Startup file: ${DEVICE_STARTUP_SCRIPT_NAME}")

# Set linker script
set(DEVICE_LINKER_FILE_STRING "${MCU_FAMILY_L}.ld")
set(DEVICE_LINKER_FILE_NAME "/GCC/${DEVICE_LINKER_FILE_STRING}")
set(DEVICE_LINKER_FILE "${DEVICE_SOURCE_DIR}/${DEVICE_LINKER_FILE_NAME}")
message("Linker File: ${DEVICE_LINKER_FILE_NAME}")

# Copy system file to build directory
configure_file(${DEVICE_SYSTEM_FILE} ${PROJECT_BINARY_DIR})
message("\r\nCopied system file to ${PROJECT_BINARY_DIR}")

# Check we set flash start variable for configuration
if(NOT FLASH_START)
    message(FATAL_ERROR "Flash start address not set!")
endif()

# Modify linker script and copy to build directory (replaces FLASH_START in the file)
configure_file(${DEVICE_LINKER_FILE} ${PROJECT_BINARY_DIR})
message("Copied linker file to ${PROJECT_BINARY_DIR}")

message("Base memory address: ${FLASH_START}")

# Add include locations
include_directories(${DEVICE_INCLUDE_DIR})

########## Outputs ##########

# Add library (where modified use copied files NOT source files)
add_library(device ${DEVICE_SYSTEM_FILE_NAME} ${DEVICE_STARTUP_SCRIPT})
set(OPTIONAL_LIBS ${OPTIONAL_LIBS} device)
