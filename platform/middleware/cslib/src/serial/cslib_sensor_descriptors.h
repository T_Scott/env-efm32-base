/**************************************************************************//**
 * Copyright 2016 by Silicon Laboratories Inc. All rights reserved.
 *
 * http://developer.silabs.com/legal/version/v11/Silicon_Labs_Software_License_Agreement.txt
 *****************************************************************************/

#ifndef __CSLIB_SENSOR_DESCRIPTORS_H__
#define __CSLIB_SENSOR_DESCRIPTORS_H__

void outputsensorDescriptors(void);

#define HAS_SENSOR_DESCRIPTORS

#define SENSOR_DESCRIPTOR_LIST "B0"

#endif // __CSLIB_SENSOR_DESCRIPTORS_H__
